---
title: "Excerise 5"
author: "Sebastian Lorek"
date: "11/22/2021"
output: pdf_document
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```

# Exercise 10.1
1.
  b) would be printed to the console;   d) would be printed to the console

2. 

```{r}
vec3 <- 0
for (i in 1:7) {
  if(vec1[i]*vec2[i]>3){
    vec3[i] <- vec1[i]*vec2[i]
  } else{
    vec3[i] <- vec1[i]+vec2[i]
  }
}
```

3. 

```{r}
mymat <- matrix(c("GREAT", "exercises", "right", "here"), ncol = 2, byrow = TRUE)
vec1 <- diag(mymat)
if(any(substr(vec1,1,1)=="G"|substr(vec1,1,1)=="g")){
  mymat <- gsub("^G|^g","HERE",mymat)
} else{
  mymat <- diag(attr(mymat,"dim")[1])
}
mymat

```

# Exercise 10.2

1.

```{r}
num <- 12
foo <- c(num, 3,21,4,21,113,0)
if(foo[1]>0 && foo[1]<=length(foo)){
  foo[foo[1]]
} else{
  foo <- NULL 
  foo
}
```

2.

```{r}
mynum <- as.integer(runif(1,0,9))
ifelse(mynum==0,"zero",switch(mynum,"one","two","three","four","five","six","seven","eight","nine"))
```

# Exercise 10.3

1.

```{r}
vec <- c(1:20)
for (i in 1:20) {
  if(vec[i]%%2==0){
    cat(vec[i], "is even\n")
  } else{
    cat(vec[i],"is odd\n")
  }
}
```

2.

```{r}
multab <- matrix(NA, ncol = 12, nrow = 12)
for (j in 1:12) {
  for (i in 1:12) {
    multab[i,j] <- i*j
  }
}
multab
```

3.

```{r}
multab2 <- matrix(NA, ncol = 12, nrow = 12)
for (i in 1:12) {
  j <- 1
  while(j+i-1<=12){
    multab2[i,(j+i-1)] <- i*(j+i-1)
    j <- j+1
  }
}
```

# Exercise 10.4

1. Given a non negative number:

```{r}
mynum <- 0
fac <- mynum
if(mynum>0){
  for(i in ((mynum-1):1)){
    fac <- fac*i
  }
  fac
} else{
  fac <- 1
}
fac-factorial(mynum)==0
```

2.

```{r}
mynum <- 10
fac <- mynum
if(mynum>0){
  i <- mynum-1
  while(i>=1){
    fac <- fac*i
    i=i-1
  }
  fac
} else{
  fac <- 1
}
fac-factorial(mynum)==0
```

3.

```{r}
mystring <- "ElAbOrAt"
index <- 1
count <- 0
result <- mystring
while (count < 2 && index <= nchar(mystring)) {
  if(substr(mystring,index,index)=="e"|substr(mystring,index
                                              ,index)=="E"){
    count <- count+1
  }
  if(count==1){
    result <- substr(mystring,1,nchar(mystring))
  } else if (count==2){
    result <- substr(mystring,1,index-1)
  } else{
    result <- NULL
  }
  
  index <- index+1
}


result
```

# Exercise 10.6

1.

```{r}
foo <- 5
bar <- c(2, 3, 1.1, 4, 0, 4.1, 3)
loop_result <- rep(NA, length(bar))
for (i in 1:length(bar)) {
  tmp <- foo / bar[i]
  if (is.finite(tmp)) {
    loop_result[i] <- tmp
  } else { 
    break
  } 
}

```






